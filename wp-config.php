<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'contango_wordpress');

/** MySQL database username */
define('DB_USER', 'contango_wordpre');

/** MySQL database password */
define('DB_PASSWORD', 'm(;Pc^;*C..J');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', TRUE); // Sucuri Security: Wed, 26 Apr 2017 23:56:59 +0000


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.]cV6qp`EBw4iQ1!6C9Tt4 yQ|V-$PI2~,#^}0vG$6oyPyF^c-]$SgA|PHT{_;#o');
define('SECURE_AUTH_KEY',  'yg W!d|]{I= KFzRZr_6D!OoU}p.L65+/rGcW*9{D6qR2hdC`r;l}65.*CF,A(2p');
define('LOGGED_IN_KEY',    'q)i%>Bj,%B+S&4vZLL>n~)?ziTkO/j(d :6FF!Wx>/B*<-cKQ+%#I54AV[&sjv,2');
define('NONCE_KEY',        'jsSpxzb;@e@>_PO*6T^#qBR+i;N2l/H#K+w>v;VMi4%e[Vk{hv5;S4g^8bwt^dbm');
define('AUTH_SALT',        '#%_)]]C+*a;g$XH=|-Tf.*aP|ri@tgByO@bPb,^37-|se=%T7@Qf>D_gK?wk,Z%+');
define('SECURE_AUTH_SALT', 'C:TS*P+RmLP_B$<*.6AAspw)-}<m_UYH$uTXB>=(%?+Or%zy?7bb{`,Po<z&#F+/');
define('LOGGED_IN_SALT',   '(l!5tg7dd3ES|-90VQ--TJ&-&Kw&_<uFUD[&V2nqr7/v.bF3KDf(D>Lq* eMjC83');
define('NONCE_SALT',       'OuF.| 5=Dl*+%AD}{Z:Ff]MB$fSmoF>h5w#qy@OD`w8W>biQ;K=9+RV$e5{@!Eff');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'conta_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

define('ONE_SIGNAL_APP_ID', 'c1b65a64-daf2-44be-b977-2bc313c06fee');
define('ONE_SIGNAL_REST_API_KEY', 'MzFhMmY1MDYtOWVjZC00ZmIyLTg5NmUtNGExNDcxZGUwYjk2');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
