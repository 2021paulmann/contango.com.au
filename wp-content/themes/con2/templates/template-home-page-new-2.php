<?php
/*
	Template Name: Home Page - New 2
*/
global $post;
get_header();
?>
<div id="wrap">
<?php get_sidebar(); ?>
<div class="container">
		
	<div id="content">
		<div class="home_banner" style="background:url(<?php echo $image[0]; ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
			
			<div class="home_banner">
		<?php echo do_shortcode('[rev_slider alias="slider-home-new-2"]');?>
			</div>
    	
		</div>				
			
		<div class="column grey_background">
			<div class="panel_content">
				<h1 class="orange">CONTANGO ASSET MANAGEMENT LIMITED</h1>
				<?php echo wpautop($post->post_content); ?>
			</div>
		</div>		
		
	</div>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>