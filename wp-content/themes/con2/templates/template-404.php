<?php /*
	Template Name: 404
*/
global $post;
get_header();?>

<div class="container">

	<?php get_sidebar(); ?>	
	
	<div id="content"style="background-color: #eee;">
		<div class="content_padding grey_background">
		<span class="four_heading">this page does not exist</span>
			<span class="four_text">Please navigate elsewhere using the menu items</span>
		</div>
		
	</div>
	<div class="clear"></div>
</div>

<?php get_footer(); ?>