<?php
/*
	Template Name: Importer
*/
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
// create a log channel
$log = new Logger('status');
$log->pushHandler(new StreamHandler( get_template_directory().'/cron.log', Logger::INFO));
// log entry
$log->addInfo( 'Import', array( 'STATUS' => 'Import script started') ); 

// URL vars
$api_key = get_query_var('key');
$year    = '0';

// Security check
if( $api_key != "9NA8C4722jf53y37O0Fv" )
	die('Invalid Request');

//$taxonomy_objects = get_object_taxonomies( 'reports', 'objects' );
   
// Lets us work with wordpress' built in database functions
global $wpdb;
// The table in the db which stores our import log
$table = 'conta_asx_ann_import_history';

///////////////////////////////////////////////////////////////////
//				
//		Check feed for any new documents
//
///////////////////////////////////////////////////////////////////
// log entry
$log->addInfo( 'Import', array( 'STATUS' => 'Import script ready to fetch new data') );

// 
$feeds    = array(
	//'ctn' => "https://wcsecure.weblink.com.au:443/clients/contango/ArchiveHeadlineJson.asmx/getHeadlines?year=".$year,
	'cie' => "https://wcsecure.weblink.com.au:443/clients/contango/cie/ArchiveHeadlineJson.asmx/getHeadlines?year=".$year,
	'cga' => "https://wcsecure.weblink.com.au:443/clients/contango/CGA/ArchiveHeadlineJson.asmx/getHeadlines?year=".$year,	
	'cqg' => "https://wcsecure.weblink.com.au:443/clients/contango/WQG/ArchiveHeadlineJson.asmx/getHeadlines?year=".$year
);

//
foreach( $feeds as $company => $feed_url )
{
	// Get the data from WebLinks feed
	$data = get_feed_data($company, $feed_url);

	// Some feeds are empty
	if( count($data) < 1 )
		continue;
	
	// Each individual file returned 
	foreach( $data as $report )
	{
		// Check if this file is unique
		$exists = $wpdb->get_row( "SELECT * FROM $table WHERE `file_id` = '$report->id' AND `company` = '$company'" );

		// Only insert if it is
		if( $exists == null )
		{
			$report_data = array(
				'file_id'  => $report->id,
				'date'     => $report->date,
				'time'     => $report->time,
				'title'    => $report->title,
				'pdf_link' => $report->pdfLink,
				'company'  => $company,
				'status'   => 'not_imported' 
			);
			
			// Write field to database
			$wpdb->insert( $table, $report_data );
		}
	}
}

// Pulls data from feed - extract json into php objects/arrays
function get_feed_data($company, $url)
{
	return json_decode(file_get_contents($url));
}



//////////////////////////////////////////////////////////////////
//
//		Process any unimported document
//
//////////////////////////////////////////////////////////////////
$unimported = $wpdb->get_results( "SELECT * FROM $table WHERE `status` = 'not_imported'" );

if( $unimported == null){
	// log entry
	$log->addInfo( 'Import', array( 'STATUS' => 'Import script found nothing to import. Script ends here.') );
	die('Nothing to Import...');
}

// log entry
$log->addInfo( 'Import', array( 'STATUS' => 'Import script start processing unimported feed items') );

foreach( $unimported as $report )
{
	$post_time = substr($report->time,0,2).':'.substr($report->time,2,2).':'.substr($report->time,4,2);
	// Insert the post into the database
	$post_id = wp_insert_post( array(
		"post_title"   => $report->title,
		"post_type"    => "reports",
		"post_status"  => "publish",
		'post_date'    => date( "Y-m-d H:i:s", strtotime( str_replace('/', '-', $report->date ).' '. $post_time ) ),
		'meta_input'   => array(
			'company' => $report->company
		)
	));
	//
	$filename = sanitize_title( $report->date.' '.$report->company.' '.$report->title ).'.pdf';
	//
	$filepath = download_pdf_from_source( $filename, $report->pdf_link );

	$type_field = wp_check_filetype( basename( $filename ), null );
	
	// Set attachment meta
	$attachment_post = array(
		 'post_title'     => $report->title,
		 'post_content'   => '', 
		 'post_status'    => 'inherit', 
		 'post_mime_type' => $type_field['type']
	);

	// Set categories
	$report_type_cat = 15; // this is the category id for asx announcements
	$period_cat_term = get_terms( array('report_periods'), array('slug' => date('Y', strtotime(str_replace('/', '-', $report->date) ) )) );
	$period_cat      = $period_cat_term[0]->term_id;
	$categories      = array();
	
	//
	$categories[0]   = wp_set_object_terms( $post_id, array((int)$period_cat),      'report_periods', true );
	$categories[1]   = wp_set_object_terms( $post_id, array((int)$report_type_cat), 'report_types',   true );
	
	// Insert attachment into media library
	$attachment      = wp_insert_attachment( $attachment_post, $filepath, $post_id );
	
	// log entry
	$log->addInfo( 'Import', array( 'STATUS' => 'Item: '.$report->title) );
	// Set company field
	//update_field( "field_563197306a6f1", $report->company, $post_id );
	
	// Set file field
	update_field( "field_5615bc77257cd", $attachment, $post_id );


	// Update log to say we've imported the file
	$wpdb->update( $table, array('status' => 'imported'), array('id' => $report->id) );
	
}

// Puts the remote pdf into uploads/year/month/filename.pdf
function download_pdf_from_source($filename, $source)
{
	//
	$filepath      = wp_upload_dir();
	$full_filepath = $filepath['path'].'/'.$filename;
	

	set_time_limit(0); // unlimited max execution time
	$options = array(
		CURLOPT_RETURNTRANSFER  => true,
		//CURLOPT_FILE            => fopen($full_filepath, "w"),
		CURLOPT_TIMEOUT         =>  28800, // set this to 8 hours so we dont timeout on big files
		CURLOPT_URL             => $source,
		CURLOPT_FOLLOWLOCATION  => true,
		//CURLOPT_HEADER          => true,
		//CURLOPT_REFERER  		=> $source
	);

	$ch = curl_init();
	curl_setopt_array( $ch, $options );
	$data = curl_exec( $ch );
	$status = curl_getinfo( $ch );
	curl_close($ch);
		
	
	/*header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Disposition: attachment; filename=$filename");
	header("Content-Type: application/pdf");
	header("Content-Transfer-Encoding: binary");*/
	$file = fopen( $full_filepath, "w+" );
	fputs( $file, $data );
	fclose( $file );
	
	if (is_readable($full_filepath))
	    return $full_filepath;
	else
		return false;
}


$log->addInfo( 'Import', array( 'STATUS' => 'Import script finished running') );