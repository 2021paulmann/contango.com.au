<?php
/*
	Template Name: Home Page
*/
global $post;
get_header();
?>
<div id="wrap">
<?php get_sidebar(); ?>
<div class="container">

		
	<div id="content">
				<div class="home_banner" style="background:url(<?php echo $image[0]; ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
			<div class="home_banner">
		<?php echo do_shortcode("[rev_slider homepage_slider]");?>
		<!--<div class="call_to_action">
			<h2>ENHANCE <span class="orange">|</span> DIVERSIFY</h2>
			<a href="" class="">FIND OUT HOW</a>
		</div>-->
	</div>
    </div>
   
    
			
			
			
		<div class="left_column lift grey_background">
			<h3 class="tab"><span class="orange">ASSET</span> MANAGEMENT</h3>
			<div class="panel_content">
				<h1 class="orange">CONTANGO ASSET MANAGEMENT LIMITED</h1>
				<?php echo wpautop($post->post_content); ?>
			</div>
		</div>
        
		
		
		<div class="right_column lift">
			<h3 class="tab"><span class="orange">LATEST</span> MEDIA</h3>
			<div class="content">
			<?php
				$the_query = new WP_Query(array(
					'post_type'      => 'media_items', 
					'posts_per_page' => 1, 
					'meta_key'       => 'type', 
					'meta_value'     => 'video' 
				));

				// The Loop
				if ( $the_query->have_posts() ):
					  	while ( $the_query->have_posts() ):
				    	$the_query->the_post();
						
											
										
						if( get_field('type', get_the_ID()) == 'video')
						{
							$background_field = get_field('video_thumbnail', get_the_ID()); 
							$background       = $background_field['sizes']['media_list'];
							
														
							echo '<a target="_blank" class="video_link" href="'.get_field('video_link', get_the_ID()).'" style="background: url('.$background.') no-repeat center center; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" class="video_link" href="'.get_field('video_link', get_the_ID()).'" class="video_link">';
							//echo '<img class="thumb" src="'..'" />';
							echo '<img src="'.IMAGES_PATH.'video_play_button.png" class="icon">';
							echo '</a>';
						}
					echo '</div>';
					echo '<div class="clear">';
					echo '</div>';
										
					echo '<div class="social_media_2">';
					echo '<h4 class=front_page style="margin-bottom:0px;border-bottom-width:0px;color:#fff;">'.get_the_title().'</h4>';
					
				
					echo '<div class="links" style="float:right;height:50px;">';
					echo '<a href="https://twitter.com/contangoam" target="_blank"><img src="http://contango.com.au/wp-content/themes/con/images/twitter_icon.gif"></a>';
					echo '<a href="https://www.linkedin.com/company/contango-asset-management-limited" target="_blank"><img src="http://contango.com.au/wp-content/themes/con/images/linkedin_icon.gif"></a>';
					echo '</div>';
					echo '</div>';
					echo '<div class="clear">';
					echo '</div>';
					endwhile;
			  	endif;
				/* Restore original Post Data */
				wp_reset_postdata();
				?>
			</div>
		
		
	</div>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>
