<?php
/*
	Template Name: Board CTN
*/
global $post;

$member_id = get_query_var('member_id');

$microcap_generator_id = 38;
$income_generator_id   = 40;
$company               = '';

$parent                = get_post($post->post_parent);
//
if( $post->post_parent == $microcap_generator_id || $post->ID == $microcap_generator_id )
	$company = 'ctn';
else if( $post->post_parent == $income_generator_id || $post->ID == $income_generator_id )
	$company = 'cie';
else
	$company = 'ctn';

get_header(); ?>

<div id="wrap">

<?php get_sidebar(); ?>

<div class="container">

	
	<div id="content" class="grey_background">
		<div class="content_padding">


<?php 
echo get_breadcrumbs($post->ID);	
	echo '<h1 class="orange">Governance</h1>';
	

	$the_query = new WP_Query( array('post_type' => 'documents', 'posts_per_page' => -1, 'tax_query' => array(array(
			'taxonomy' => 'document_category',
			'field'    => 'slug',
			'terms'    => 'governance',
		)) ) );

	// The Loop
	if ( $the_query->have_posts() ):
		echo '<ul class="media_list">';
	  	while ( $the_query->have_posts() ):
			$the_query->the_post();
			
			$link = get_field('file', get_the_ID());
			
			echo '<li>';
			
			echo '<div class="listing">';
			echo '<a target="_blank" href="'.$link.'">';
			echo '	<h4 class="orange">'.get_the_title().'</h4>';

			echo '<p class="terms">PDF Download</p>';
			echo '	<div class="icon pdf"></div>';
			echo '</a>';
			echo '</div>';
			echo '</li>';
		endwhile;
		echo '</ul>';
  	endif;
	/* Restore original Post Data */
	//wp_reset_postdata();
	echo '</div>';
	echo '<div class="clear"></div>';
		
 ?>

 		</div>
		
 	</div>
	
 	<div class="clear"></div>
 </div>
</div>

 

<?php get_footer(); ?>