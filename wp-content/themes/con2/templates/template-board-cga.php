<?php
/*
	Template Name: Board CGA
*/
global $post;

$member_id = get_query_var('member_id');

$microcap_generator_id = 38;
$income_generator_id   = 40;
$investor_centre_id	   = 12877;
$company               = '';

$parent                = get_post($post->post_parent);
//
if( $post->post_parent == $microcap_generator_id || $post->ID == $microcap_generator_id )
	$company = 'ctn';
else if( $post->post_parent == $income_generator_id || $post->ID == $income_generator_id )
	$company = 'cie';
else if( $post->post_parent == $investor_centre_id || $post->ID == $investor_centre_id )
	$company = 'cga';
else
	$company = 'ctn';

get_header(); ?>
<div id="wrap">

<?php get_sidebar(); ?>



<div class="container">
	
	<div id="content" class="grey_background">
		<div class="content_padding">


<?php 
echo get_breadcrumbs($post->ID);	
if( $member_id == '' )
{
	echo '<div class="left_column">';
	echo '<h1 class="orange">CONTANGO ASSET MANAGEMENT LIMITED BOARD</h1>';
	/*
		Show the list of board members
	*/
	$the_query = new WP_Query( array('post_type' => 'team', 'posts_per_page' => -1, 'tax_query' => array(array(
			'taxonomy' => 'team_role',
			'field'    => 'slug',
			'terms'    => 'board-of-directors'
		)),
		'meta_query'	=> array(
			array(
				'key'		=> 'company',
				'value'		=> $company,
				'compare'	=> 'LIKE'
			)
		)
	));

	// The Loop
	if ( $the_query->have_posts() ):
		$i = 1;
	  	while ( $the_query->have_posts() ):
	    	$the_query->the_post(); 
			$class = ($i & 2 == 0)? 'last':'';
			echo '<div class="member '.$class.'">';
			echo '	<a href="'.site_url()."/".$parent->post_name."/board-governance/?member_id=".get_the_ID().'">';
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'team_thumb' );
			echo '<img src="'.$image[0].'" class="" />';
			
			echo '	<div class="more_info">MORE INFORMATION</div>';
			echo '	<h4>'.get_the_title().'</h4>'; 
			echo '	<p class="qualifications">'.get_field('qualifications', get_the_ID()).'</p>';
			echo '	<p class="role">'.get_field('role', get_the_ID()).'</p>';
			echo '	</a>';
			echo '</div>';
			$i++;
		endwhile;
	endif;	
	echo '<div class="clear"></div>';
	echo '</div>';
	echo '<div class="right_column left_margin">';
	echo '<h1 class="orange">Governance</h1>';
	

	$the_query = new WP_Query( array('post_type' => 'documents', 'posts_per_page' => -1, 'tax_query' => array(array(
			'taxonomy' => 'document_category',
			'field'    => 'slug',
			'terms'    => 'cga-governance',
		)) ) );

	// The Loop
	if ( $the_query->have_posts() ):
		echo '<ul class="media_list">';
	  	while ( $the_query->have_posts() ):
			$the_query->the_post();
			
			$link = get_field('file', get_the_ID());
			
			echo '<li>';
			
			echo '<div class="listing">';
			echo '<a target="_blank" href="'.$link.'">';
			echo '	<h4 class="orange">'.get_the_title().'</h4>';

			echo '<p class="terms">PDF Download</p>';
			echo '	<div class="icon pdf"></div>';
			echo '</a>';
			echo '</div>';
			echo '</li>';
		endwhile;
		echo '</ul>';
  	endif;
	/* Restore original Post Data */
	//wp_reset_postdata();
	echo '</div>';
	echo '<div class="clear"></div>';
		
}
else
{ 
	/*
		Show the individual team member profile
	*/
	$the_query = new WP_Query( array('post_type' => 'team', 'posts_per_page' => 1, 'p' => $member_id ) );

	// The Loop
	if ( $the_query->have_posts() ):
		$i = 1;
	  	while ( $the_query->have_posts() ):
	    	$the_query->the_post(); 
			
			echo '<h1 class="orange">'.get_the_title().'</h1>';
			
			echo '<p class="orange qualification">'.get_field('qualifications', get_the_ID()).'</p>';
			
			echo '<p class="role">'.get_field('role', get_the_ID()).'</p>';
			
			echo '<div class="left_column one_third">';
			echo get_field('left_column', get_the_ID());
			echo '</div>';
			
			echo '<div class="left_column one_third">';
			echo get_field('right_column', get_the_ID());
			echo '</div>';
			
			echo '<div class="left_column one_third last">';
			
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'team_thumb' );
			echo '<div class="profile_photo">';
			echo '	<img src="'.$image[0].'" class="" />';
			if( get_field('linkedin_url', get_the_ID()) || get_field('twitter_url', get_the_ID()) )
			{
				echo '<div class="social_buttons">';
				
					if( get_field('linkedin_url', get_the_ID()) )
						echo '<a href="'.get_field('linkedin_url', get_the_ID()).'" class="linkedin"></a>';
					if( get_field('twitter_url', get_the_ID()) )
						echo '<a href="'.get_field('twitter_url', get_the_ID()).'" class="twitter"></a>';
				
				echo '</div>';
			}
			echo '</div>';
				
			echo '</div>';
				
			echo '<div class="clear"></div>';
					
		endwhile;
	endif;
 } ?>
 



 		</div>
		
 	</div>
	
 	<div class="clear"></div>
 </div>
</div>
 
 
 

<?php get_footer(); ?>