<?php
/*
	Template Name: Annual Reports
*/
global $post;
get_header(); 

//$report_type = get_query_var('report_type');
//$date_period = get_query_var('date_period');
$microcap_generator_id = 38;
$income_generator_id   = 40;
$investor_centre_id	   = 12877;
$global_growth_id	   = 14112;
$company               = '';
$offset                = ( get_query_var('offset') != '' ) ? (int)get_query_var('offset') : 0;
$per_page              = 20;

$parent                = get_post($post->post_parent);
//
if( $post->post_parent == $microcap_generator_id || $post->ID == $microcap_generator_id )
	$company = 'ctn-ar';
else if( $post->post_parent == $income_generator_id || $post->ID == $income_generator_id )
	$company = 'cie-ar';
else if( $post->post_parent == $investor_centre_id || $post->ID == $investor_centre_id )
	$company = 'cga-ar';
else if( $post->post_parent == $global_growth_id || $post->ID == $global_growth_id )
	$company = 'cqg-ar';
else
	$company = 'ctn';

?>
<div id="wrap">
	<?php get_sidebar(); ?>



<div class="container">
	
	<div id="content" class="grey_background">
		<div class="content_padding">
			<?php echo get_breadcrumbs($post->ID); ?>
			<h1 class="orange">Annual Reports</h1>
			<?php 		
			/*
			
			*/
			$args      =  array('post_type' => 'research_documents', 'posts_per_page' => -1, 'offset' => $offset, 'meta_key' => 'company', 'meta_value' => $company);
			$total     =  count( get_posts($args) );
			// Now restrict to pagination
			$args['posts_per_page'] = $per_page;
			$the_query = new WP_Query($args);

			// The Loop
			if ( $the_query->have_posts() ):
			
				//echo get_pagination_links($offset, $per_page, $total);
				
				
				/*
			
				*/
				echo '<table id="reports">';
				echo '	<thead>';
				echo '		<tr>';
				echo '			<td style="width:10%;">DATE</td>';
				echo '			<td style="width:100%;">DESCRIPTION</td>';
				echo '			<td style="text-align:center; padding-left:0px;width:10%;">DOCUMENT</td>';
				echo '		</tr>';
				echo '	</thead>';
				echo '	<tbody>';
			
			
			
			  	while ( $the_query->have_posts() ):
			    	$the_query->the_post(); 
					echo '<tr>';
					echo '	<td style="text-align:left;width:10%;">'.get_the_date('d/m/y').'</td>';
					echo '	<td style="width:80%;">'.get_the_excerpt().'</td>';
					echo '	<td style="text-align:center;width:10%;"><a href="'.get_field('file', get_the_ID()).'" target="_blank">VIEW</a></td>';
					echo '</tr>';
				endwhile;
				
				
				echo '	</tbody>';
				echo '</table>';
				
				echo '<div class="clear"></div>';
			else:
				echo '<p class="para_size">No Annual Reports Currently Available - Check Back Soon</p>';
			endif; ?>

			
		</div>
		
	</div>
	<div class="clear"></div>
</div>
</div>

<?php get_footer(); ?>