<?php 
/*
	Template Name: Dividend History - Microcap
*/
global $post;
get_header(); 
?>
<div id="wrap">
	<?php get_sidebar(); ?>
<div class="container">	
	
	<div id="content"style="background-color: #eee;">
		<?php
		if( have_posts() ):
			while( have_posts()): the_post();
				if( get_field('show_banner', get_the_ID())):
					echo '<div class="page_banner">';
					if( has_post_thumbnail() ) 
						the_post_thumbnail('page_banner');
					
					if( get_field( 'banner_message', get_the_ID() ) )
						echo '<h3><span class="orange">|</span> '.get_field( 'banner_message', get_the_ID() ).'</h3>';
				
					echo '</div>';
				endif;
						
				
				echo '<div class="content_padding grey_background">';
					echo get_breadcrumbs( get_the_ID() );
					echo '<h1 class="orange">DIVIDEND HISTORY</h1>';
					the_content();
	echo '<div class="left_column">';
	echo '</div>';
	endwhile;
	endif;
	echo '<div class="clear"></div>';
	echo '</div>';
	echo '<div class="right_column left_margin">';
	

	$the_query = new WP_Query( array('post_type' => 'documents', 'posts_per_page' => -1, 'tax_query' => array(array(
			'taxonomy' => 'document_category',
			'field'    => 'slug',
			'terms'    => 'dividend-history-microcap',
		)) ) );

	// The Loop
	if ( $the_query->have_posts() ):
		echo '<ul class="media_list">';
	  	while ( $the_query->have_posts() ):
			$the_query->the_post();
			
			$link = get_field('file', get_the_ID());
			
			echo '<li>';
			
			echo '<div class="listing">';
			echo '<a target="_blank" href="'.$link.'">';
			echo '	<h4 class="orange">'.get_the_title().'</h4>';

			echo '<p class="terms">PDF Download</p>';
			echo '	<div class="icon pdf"></div>';
			echo '</a>';
			echo '</div>';
			echo '</li>';
		endwhile;
		echo '</ul>';
  	endif;
	/* Restore original Post Data */
	//wp_reset_postdata();
	echo '</div>';
	echo '<div class="clear"></div>';
							echo '<div class="clear"></div>';
				echo '</div>';
		?>
	</div>
	<div class="clear"></div>
</div>
</div>

<?php get_footer(); ?>