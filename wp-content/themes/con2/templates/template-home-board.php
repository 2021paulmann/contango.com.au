<?php
/*
	Template Name: Home Board
*/
global $post;

$member_id = get_query_var('member_id');

get_header(); ?>
<div id="wrap">

<?php get_sidebar(); ?>

<div class="container">



	<div id="content" class="grey_background">
		<div class="content_padding">


<?php
echo get_breadcrumbs($post->ID);
if( $member_id == '' )
{
	echo '<h1 class="orange">CONTANGO ASSET MANAGEMENT LIMITED BOARD OF DIRECTORS</h1>';
	/*
		Show the list of team members
	*/
	$the_query = new WP_Query( array('post_type' => 'team', 'posts_per_page' => -1, 'tax_query' => array(array(
			'taxonomy' => 'team_role',
			'field'    => 'slug',
			'terms'    => 'home-board',
		)), 'orderby' => 'date', 'order'   => 'DEC',));

	// The Loop
	if ( $the_query->have_posts() ):
		$i = 1;
	  	while ( $the_query->have_posts() ):
	    	$the_query->the_post();
			$class = ($i & 4 == 0)? 'last':'';
			echo '<div class="member '.$class.'">';
			echo '	<a href="'.site_url()."/cga-board/".get_the_ID().'">';
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'team_thumb' );
			echo '<img src="'.$image[0].'" class="" />';

			echo '	<div class="more_info">MORE INFORMATION</div>';
			echo '	<h4>'.get_the_title().'</h4>';
			echo '	<p class="qualifications">'.get_field('qualifications', get_the_ID()).'</p>';
			echo '	<p class="role">'.get_field('role', get_the_ID()).'</p>';
			echo '	</a>';
			echo '</div>';
			$i++;
		endwhile;
	endif;

	echo '<div class="clear"></div>';

}
else
{
	/*
		Show the individual team member profile
	*/
	$the_query = new WP_Query( array('post_type' => 'team', 'posts_per_page' => 1, 'p' => $member_id ) );

	// The Loop
	if ( $the_query->have_posts() ):
		$i = 1;
	  	while ( $the_query->have_posts() ):
	    	$the_query->the_post();

			echo '<h1 class="orange">'.get_the_title().'</h1>';

			echo '<p class="orange qualification">'.get_field('qualifications', get_the_ID()).'</p>';

			echo '<p class="role">'.get_field('role', get_the_ID()).'</p>';

			echo '<div class="left_column one_third">';
			echo get_field('left_column', get_the_ID());
			echo '</div>';

			echo '<div class="left_column one_third">';
			echo get_field('right_column', get_the_ID());
			echo '</div>';

			echo '<div class="left_column one_third last">';

			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'team_thumb' );
			echo '<div class="profile_photo">';
			echo '	<img src="'.$image[0].'" class="" />';
			if( get_field('linkedin_url', get_the_ID()) || get_field('twitter_url', get_the_ID()) )
			{
				echo '<div class="social_buttons">';

					if( get_field('linkedin_url', get_the_ID()) )
						echo '<a href="'.get_field('linkedin_url', get_the_ID()).'" class="linkedin"></a>';
					if( get_field('twitter_url', get_the_ID()) )
						echo '<a href="'.get_field('twitter_url', get_the_ID()).'" class="twitter"></a>';

				echo '</div>';
			}
			echo '</div>';

			echo '</div>';

			echo '<div class="clear"></div>';

		endwhile;
	endif;
 } ?>




 		</div>

 	</div>
	</div>

 	<div class="clear"></div>
 </div>




<?php get_footer(); ?>
