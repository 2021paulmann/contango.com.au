<?php
/*
	Template Name: CTN Home
*/
global $post;
get_header();
?>
<div id="wrap">
<?php get_sidebar(); ?>

<div class="container">
    
    <div id="content">
			<div class="page_banner" style="background:url(<?php echo $image[0]; ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
			<div class="page_banner">
				<?php echo do_shortcode("[rev_slider ctn_home]");?>
			</div>
    </div>
   	
		<?php
					echo '<div class="content_padding grey_background">';
					echo get_breadcrumbs( get_the_ID() );
					the_content();
					echo '<div class="clear"></div>';
				echo '</div>';
			?>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
</div>

<?php get_footer(); ?>