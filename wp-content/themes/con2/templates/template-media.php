<?php
/*
	Template Name: Media
*/
global $post;
get_header(); 

$category  = get_query_var('media_category');
$period    = get_query_var('date_period');


?>
<div id="wrap">

<?php get_sidebar(); ?>

<div class="container">

	
	
	<div id="content" class="grey_background">
		<div class="content_padding">
			<?php echo get_breadcrumbs($post->ID); ?>
			<h1 class="orange">LATEST MEDIA</h1>
			<?php 
			echo '<form action="'.site_url().'/media" type="GET">';
			/*
			
			*/
			$taxonomies = array('media_categories');
			$args       = array(
			    'orderby'           => 'id', 
			    'order'             => 'ASC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '',
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0,
			    'childless'         => false,
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 
			$terms = get_terms($taxonomies, $args);
			echo '<select id="" name="media_category">';
			echo '	<option value="all">All Categories</option>';
			foreach( $terms as $type ):
				echo '<option value="'.$type->slug.'" '.(($category == $type->slug )?'selected':'').'>'.$type->name.'</option>';
			endforeach;
			echo '</select>';
			
			
			
			
			
 		   	/*
			
			*/
			$taxonomies = array('date_periods');
			$args = array(
			    'orderby'           => 'id', 
			    'order'             => 'ASC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '',
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0,
			    'childless'         => false,
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 
			$terms = get_terms($taxonomies, $args);
			echo '<select id="" name="date_period">';
			echo '	<option value="all">All Dates</option>';
			foreach( $terms as $type ):
				echo '<option value="'.$type->slug.'" '.(($period == $type->slug )?'selected':'').'>'.$type->name.'</option>';
			endforeach;
			echo '</select>';
			
			
			echo '<input type="submit" class="orange_button" value="SUBMIT" />';
			echo '</form>';
			
			
			
			/*
			
			*/
			$tax_query = array();
			//
			if( $category !== '' && $category !== 'all')
			{
				array_push($tax_query, array(
					'taxonomy' => 'media_categories',
					'field'    => 'slug',
					'terms'    => array( $category ),
				));
			}
			// 
			if( $period !== '' && $period !== 'all')
			{
				array_push($tax_query, array(
					'taxonomy' => 'date_periods',
					'field'    => 'slug',
					'terms'    => array( $period ),
				));
				
				if( count( $tax_query ) > 1 )
					$tax_query['relation'] = 'AND';
			}
			
			//pr( $tax_query);
			/*
			
			*/
			$the_query = new WP_Query( array('post_type' => 'media_items', 'posts_per_page' => -1, 'tax_query' => $tax_query ) );

			// The Loop
			if ( $the_query->have_posts() ):
				$i = 1;
				// The Loop
				echo '<ul class="media_list">';
			  	while ( $the_query->have_posts() ):
					$the_query->the_post();
					
					
					$link = '';
					if( get_field('type', get_the_ID()) == 'video')
						{$link = get_field('video_link', get_the_ID());}
					else if( get_field('type', get_the_ID()) == 'pdf')
						{$link = get_field('pdf', get_the_ID());}
					else if( get_field('type', get_the_ID()) == 'link')
						{$link = get_field('link', get_the_ID());}
					
					
					echo '<li class="'.(($i % 2 == 0)?'last':'').'">';
				
			    	
					if( get_field('type', get_the_ID()) == 'video')
					{
						$src_field = get_field('video_thumbnail', get_the_ID());
						// $src       = $src_field['sizes']['media_list'];
						////////// Above and below are attempts to stop errors in the error log!!!!!!
						// echo '<a target="_blank" class="video_link" href="'.get_field('video_link', get_the_ID()).'" style="background: url('.$src.'); -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">';
						echo '<a target="_blank" class="video_link" href="'.get_field('video_link', get_the_ID()).'">';
						//echo '	<img class="thumb" src="'..'" />';
						echo '	<img src="'.IMAGES_PATH.'video_play_button.png" class="icon">';
						echo '</a>';
					}
					echo '<div class="listing">';
					echo '	<a target="_blank" href="'.$link.'">';
					echo '	<h4 class="orange">'.get_the_title().'</h4>';
				
					$terms       = get_the_terms( get_the_ID(), 'media_categories' );
					$term_string = '';
					if( count( $terms ) > 0 && is_array($terms) ):
						foreach( $terms as $key => $term ):
							$term_string .= $term->name;
							$term_string .= ($key < (count($terms)-1) ) ? ' | ':'';
						endforeach;
					endif; 
				
					echo '			<p class="terms">'.$term_string.'</p>';
					echo '		</a>';
					echo '	</div>';
					
					echo '<div class="description">';
					echo get_the_content();
					echo '</div>';
					echo '<a target="_blank" href="'.$link.'" class="orange_button">MORE</a><span class="">'.get_the_date().'</span>';
					
					echo '</li>';
					$i++;
				endwhile;
				echo '</ul>';
			else:
				echo '<p class="no_results">No Media Found</p>';
			endif; ?>
		</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<?php get_footer(); ?>
