<?php 
/*
	Template Name: ASX Feeds
*/


get_header(); ?>

<div class="container">

	<?php get_sidebar(); ?>
	
	
	<div id="content">
		<div class="full_asx_content content_padding grey_background">
			<?php echo indices_table( "S&amp;P/ASX INDICES", 'asx_indices' ); ?>
		
			<?php echo indices_table( "WORLD INDICES", 'world_indices' ); ?>
		
			<?php echo indices_table( "METALS &amp; CURRENCIES", 'metal_currency' ); ?>
		</div>
		
	</div>
	<div class="clear"></div>
</div>

<?php get_footer(); ?>
