<?php
/*
	Template Name: Reports
*/
global $post;
get_header(); 

$report_type = get_query_var('report_type');
$date_period = get_query_var('date_period');
$offset      = ( get_query_var('offset') != '' ) ? (int)get_query_var('offset') : 0;
$per_page    = 20;
$parent      = get_post($post->post_parent);
$company     = get_page_section_company($post, 'ctn');


?>
<div id="wrap">

<?php get_sidebar(); ?>

<div class="container">

	
	<div id="content" class="grey_background">
		<div class="content_padding">
			<?php echo get_breadcrumbs($post->ID); ?>
			<h1 class="orange">ANNOUNCEMENTS & REPORTS</h1>
			<?php 
			echo '<form action="" type="GET">';
			/*
			
			*/
			$taxonomies = array('report_types');
			$args       = array(
			    'orderby'           => 'id', 
			    'order'             => 'ASC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '',
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0,
			    'childless'         => false,
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 
			$terms = get_terms($taxonomies, $args);
			echo '<select id="" name="report_type">';
			echo '	<option value="all">All Reports</option>';
			foreach( $terms as $type ):
				echo '<option value="'.$type->slug.'" '.(($report_type == $type->slug )?'selected':'').'>'.$type->name.'</option>';
			endforeach;
			echo '</select>';
			
			
			
			echo '<input type="hidden" name="offset" value="'.$offset.'" />';
			
			
 		   	/*
			
			*/
			$taxonomies = array('report_periods');
			$args = array(
			    'orderby'           => 'slug', 
			    'order'             => 'DESC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '',
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0,
			    'childless'         => false,
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 
			$terms = get_terms($taxonomies, $args);
			echo '<select id="" name="date_period">';
			echo '	<option value="all">All Dates</option>';
			foreach( $terms as $type ):
				echo '<option value="'.$type->slug.'" '.(($date_period == $type->slug )?'selected':'').'>'.$type->name.'</option>';
			endforeach;
			echo '</select>';
			
			
			echo '<input type="submit" class="orange_button" value="SUBMIT" />';
			echo '</form>';
			
			
			
			/*
			
			*/
			$tax_query = array();
			//
			if( $report_type !== '' && $report_type !== 'all')
			{
				array_push($tax_query, array(
					'taxonomy' => 'report_types',
					'field'    => 'slug',
					'terms'    => array( $report_type ),
				));
			}
			// 
			if( $date_period !== '' && $date_period !== 'all')
			{
				array_push($tax_query, array(
					'taxonomy' => 'report_periods',
					'field'    => 'slug',
					'terms'    => array( $date_period ),
				));
				
				if( count( $tax_query ) > 1 )
					$tax_query['relation'] = 'AND';
			}

			/*
			
			*/
			// Get the total
			//echo "<!-- " . $offset. " " . $tax_query . " " . $company . "-->";

			$args      = array('post_type' => 'reports', 'posts_per_page' => -1, 'offset' => $offset, 'tax_query' => $tax_query, 'meta_key' => 'company', 'meta_value' => $company );
			$total     = count( get_posts($args ) );
			// Now restrict to pagination
			$args['posts_per_page'] = $per_page;
			//var_dump($args);

			$the_query = new WP_Query( $args );

			// The Loop
			
			if ( $the_query->have_posts() ):
			
				
				//echo get_pagination_links($offset, $per_page, $total);
				
					
				/*
			
				*/
				echo '<table id="reports">';
				echo '	<thead>';
				echo '		<tr>';
				echo '			<td style="width:10%;">DATE</td>';
				echo '			<td style="width:80%;">DESCRIPTION</td>';
				echo '			<td style="text-align:center; padding-left:0px;">DOCUMENT</td>';
				echo '		</tr>';
				echo '	</thead>';
				echo '	<tbody>';
			
			
			
			  	while ( $the_query->have_posts() ):
			    	$the_query->the_post(); 
					echo '<tr>';
					echo '	<td style="text-align:left; width:10%">'.get_the_date('d/m/y').'</td>';
					echo '	<td style="width:100%;">'.get_the_title().'</td>';
					echo '	<td style="text-align:center;"><a href="'.get_field('file', get_the_ID()).'" target="_blank">VIEW</a></td>';
					echo '</tr>';
				endwhile;
				
				
				echo '	</tbody>';
				echo '</table>';
				
				echo get_pagination_links($offset, $per_page, $total);
				echo '<div class="clear"></div>';
			else:
				echo '<p class="para_size">No announcements found, please check back soon.</p>';
			endif; ?>
			
		</div>
	</div>
	<div class="clear"></div>
</div>
</div>

<?php get_footer(); ?>