<?php
require( TEMPLATEPATH .'/vendor/autoload.php' );
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Add Page ID to admin
function cf_post_id() {
	global $post;

	// Get the data
	$id = $post->ID;

	// Echo out the field
	echo '<input type="text" name="_id" value="' . $id . '" class="widefat disabled" />';
}

function ve_custom_meta_boxes() {
	add_meta_box('projects_refid', 'Post ID', 'cf_post_id', 'post', 'side', 'high');
	add_meta_box('projects_refid', 'Page ID', 'cf_post_id', 'page', 'side', 'high');
}
add_action('add_meta_boxes', 've_custom_meta_boxes');

// Separate our various scripts to keep functions.php clean
include( TEMPLATEPATH . '/scripts/constants.php' );
include( TEMPLATEPATH . '/scripts/routes.php' );
include( TEMPLATEPATH . '/scripts/widgets.php' );
include( TEMPLATEPATH . '/scripts/menus.php' );
include( TEMPLATEPATH . '/scripts/custom_post_types.php' );
include( TEMPLATEPATH . '/scripts/taxonomies.php' );
include( TEMPLATEPATH . '/scripts/hooks.php' );
include( TEMPLATEPATH . '/scripts/shortcodes.php' );
include( TEMPLATEPATH . '/scripts/rest_routes.php' );

// Hide admin bar from frontend
// add_filter('show_admin_bar', '__return_false');

// Let us set featured image for posts/pages/custom post types
add_theme_support( 'post-thumbnails' );

// Enqueue our script so that there's no conflict with jquery
add_action( 'wp_enqueue_scripts', 'load_scripts' );
function load_scripts()
{
	
	//wp_enqueue_style('mainstyle', CSS_PATH. 'style.css', '', rand(10,1000), 'all');
	wp_enqueue_style('mainstyle', CSS_PATH. 'style.css', '', null, 'all');
	/* Loads the file containing our version of jquery, but the file also contains any other plugins or libraries we might need */
	wp_enqueue_script('jquery', JS_PATH.'tools.js');
}


// WHat sizes to we want to make images when we upload them
add_image_size ( 'team_thumb', 200, 200, array( 'center', 'center' ) );
add_image_size ( 'media_thumb', 423, 216, array( 'center', 'center' ) );
add_image_size ( 'media_list', 500, 295, array( 'center', 'center' ) );
add_image_size ( 'page_banner', 1000, 360, array( 'center', 'center' ) );
//add_image_size ( 'project_full',  460, 330, array( 'center', 'center' ) );
//add_image_size ( 'product_full',  688, 494, array( 'center', 'center' ) );
//add_image_size ( 'product_full_portrait',  460, 690, false );

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

function pr( $data, $string = false)
{
	echo '<pre>';
	print_r( $data, $string );
	echo '</pre>';
}
// Fix Gravity forms issue with conditional logic
add_filter('gform_init_scripts_footer', 'init_scripts');
function init_scripts() {
    return true;
}

/**
* When post change status from daft t- publish for some post types
*/
add_action( 'auto-draft_to_publish', 'publish_post_notification', 10, 1 );
add_action( 'new_to_publish', 'publish_post_notification', 10, 1 );
add_action( 'draft_to_publish', 'publish_post_notification', 10, 1 );
function publish_post_notification( $post ){
	
	$post_type = get_post_type( $post );	
	$post_title = get_the_title( $post->ID );
	
	if ( 
		$post_type == 'media_items' || $post_type == 'reports' || 
		$post_type == 'research_documents' || $post_type == 'documents'
	){	
		$message = '';
		// create a log channel
		$log = new Logger('response');
		$log->pushHandler(new StreamHandler( get_template_directory().'/notification.log', Logger::INFO));
		
		if( $post_type == 'media_items' ){
			$message = 'Media: '. $post_title;
		}
		if( $post_type == 'reports' ){
			$message = 'Report: '. $post_title;
		}
		/*if( $post_type == 'research_documents' ){
			$message = 'Research document: '. $post_title;
		}
		if( $post_type == 'documents' ){
			$message = 'Document: '. $post_title;
		}*/
		
		$log->addInfo( 'New post publish hook fire event: '.$message );
		
		$send_notification = true; // default is true
		if( isset($_POST['fields']) && isset($_POST['fields']['field_563197306a6f1']) ){
			$company = $_POST['fields']['field_563197306a6f1'];
			if( $company == 'cqg' ){
				$send_notification = false;
			}
		}elseif( get_field('company', $post->ID ) ){
			$company = get_field('company', $post->ID ); 
			if( $company == 'cqg' ){
				$send_notification = false;
			}
		}elseif( get_field_object( 'field_563197306a6f1', $post->ID ) ){
			$company = get_field_object( 'field_563197306a6f1', $post->ID );
			if( $company == 'cqg' ){
				$send_notification = false;
			}
		}

		if( $send_notification ){
			$response = sendMessage( $message ); 
			// add records to the log
			$log->addInfo( $message, array( 'response' => $response ) );
		}else{
			$log->addInfo( 'Notification not sent as post | '.$message.' | is from CQG' );
		}		

	} 
}
