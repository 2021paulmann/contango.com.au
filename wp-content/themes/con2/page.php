<?php get_header(); ?>
<div id="wrap">

<?php get_sidebar(); ?>

<div class="container">
	
	
	<div id="content">
		<?php
		if( have_posts() ):
			while( have_posts()): the_post();
				if( get_field('show_banner', get_the_ID())):
					echo '<div class="page_banner">';
					if( has_post_thumbnail() ) 
						the_post_thumbnail('page_banner');
					
					if( get_field( 'banner_message', get_the_ID() ) )
						echo '<h3><span class="orange">|</span> '.get_field( 'banner_message', get_the_ID() ).'</h3>';
				
					echo '</div>';
				endif;
						
				
				echo '<div class="content_padding grey_background">';
					echo get_breadcrumbs( get_the_ID() );
					the_content();
					echo '<div class="clear"></div>';
				echo '</div>';
			endwhile;	
		endif;
		?>
	</div>
	</div>
	<div class="clear"></div>
</div>

<?php get_footer(); ?>