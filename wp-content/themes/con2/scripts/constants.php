<?php
	// Basic
	define( 'BASE_URL',           get_bloginfo('url') );
	
	// PATHS
	define( 'THEME_PATH',        get_template_directory_uri() );
	define( 'CSS_PATH',          get_template_directory_uri().'/css/' );
	define( 'JS_PATH',           get_template_directory_uri().'/js/' );
	define( 'IMAGES_PATH',       get_template_directory_uri().'/images/' );


