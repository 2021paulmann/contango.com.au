<?php

function get_page_section_company($post, $default = 'cga')
{

	$microcap_generator_id = 38; // page id for http://contango.com.au/listed-companies/contango-microcap-limited/
	$income_generator_id   = 40; // page id for http://contango.com.au/listed-companies/contango-income-generator/
	$asset_management_id   = 6;  // page id for http://contango.com.au/asset-management/
	$investor_center	   = 12877; // page id for http://contango.com.au/investor-centre/
	$global_growth_id	   = 14112; // page id for http://contango.com.au/listed-companies/global-growth-limited/
	$company               = '';
	$parent                = get_post($post->post_parent);
	//
	/*if( $post->post_parent == $microcap_generator_id || $post->ID == $microcap_generator_id )
		$company = 'ctn';
	else*/ if( $post->post_parent == $income_generator_id || $post->ID == $income_generator_id )
		$company = 'cie';
	else if( $post->post_parent == $investor_center || $post->ID == $investor_center )
		$company = 'cga'; 
	else if( $post->post_parent == $global_growth_id || $post->ID == $global_growth_id )
		$company = 'cqg';
	else if( $post->post_parent == $asset_management_id || $post->ID == $asset_management_id )
		$company = 'aml';
	else
		$company = $default;
	

	return $company;
}// end func


function get_feed($feed, $decode = false)
{

	$url = '';
	switch( $feed )
	{

		/*case 'ctn_price':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/pricejson.asmx/getQuote?';
		break;*/
		case 'cie_price':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/cie/pricejson.asmx/getQuote?';
		break;
		case 'cga_price':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/CGA/pricejson.asmx/getQuote?';
		break;
		/* CQG price replaced with link for the WQG price */
		case 'cqg_price':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/WQG/pricejson.asmx/getQuote?';
		break;
		case 'asx_indices':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/AsxIndicesJSON.asmx/getQuote?';
		break;
		case 'world_indices':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/WorldIndicesJSON.asmx/getQuote?';
		break;
		case 'metal_currency':
			$url = 'https://wcsecure.weblink.com.au:443/clients/contango/MetalCurrencyJSON.asmx/getQuote?';
		break;
	}

	if( $decode )
		return json_decode(file_get_contents( $url ));
	else
		return file_get_contents( $url );
}// end func

function indices_table( $title, $feed ){

	$indices = json_decode(get_feed($feed)); 
	$html    = '';

	$html .= '<h3>'.$title.'</h3>';
	$html .= '<table style="width:100%">';
	$html .= '	<thead>';
	$html .= '		<tr><td>Name</td><td>Last</td><td></td><td>Change</td></tr>';
	$html .= '	</thead>';
	$html .= '	<tbody>';
	if( isset($indices->quote) ){
		foreach( $indices->quote as $index ){
			$arrow = ( ( $index->changePercent > 0 ) ? 'up' : 'down' );
			$html .= '<tr>';
			$html .= '	<td style="width:25%">'.$index->name.'</td>';
			$html .= '	<td style="width:35%">'.$index->last.'</td>';
			$html .= '	<td style="width:15%"><img src="'.IMAGES_PATH.'index_arrow_'.$arrow.'.png" alt="'.$arrow.'"></td>';
			$html .= '	<td style="width:25%">'.$index->changePercent.'</td>';
			$html .= '</tr>';
		}
	}

	$html .= '	</tbody>';
	$html .= '</table>';
	
	return $html;
}// end func



function get_price( $feed ){
	return '$'.json_decode(get_feed($feed))->quote[0]->lastprice;
}// end func

function get_breadcrumbs( $post_id ){

	//
	$the_post  = get_post( $post_id );
	$ancestors = get_post_ancestors( $the_post );
	$ancestry  = '';
	//
	$html = '<div class="breadcrumbs">';
	//
	$html .= ' <a href="'.site_url().'">HOME /</a> ';
	//
	foreach( $ancestors as $ancestor ){

		$ancestor_post = get_post( $ancestor );
		//
		$html .= '<a href="'.site_url().'?page_id='.$ancestor_post->ID .'/">'.$ancestor_post->post_title.' / </a>';

	}

	//
	$html .= ' <a href="'.site_url().'/?page_id='.$the_post->ID.'">'.$the_post->post_title.'</a> ';
	//
	$html .= '</div>';
	//
	return $html;
}// end func

function get_pagination_links($offset, $posts_per_page, $total){

	// 
	$html       = '';
	$uri_string = $_SERVER['REQUEST_URI'];
	$pattern    = "/offset=(\d*)/";
	//
	$next       = $posts_per_page + $offset;
	$prev       = $offset - $posts_per_page;
	//
	if( $prev < 1 )
		$prev = 0;

	// Offset hasn't been set yet
	if( strpos( $uri_string, 'offset=') === FALSE )
	{
		$separator = (parse_url($uri_string, PHP_URL_QUERY) == NULL) ? '?' : '&';
		//
		$next_url = $uri_string .$separator."offset=$next";
		$prev_url = $uri_string .$separator.'offset=0';
	}
	else
	{
		// Replace the offset variable with the next and previous values
	    $next_url = preg_replace($pattern, "offset=$next", $uri_string, 1);
		$prev_url = preg_replace($pattern, "offset=$prev", $uri_string, 1);
	}

	// Calculate how to describe the currently viewed number
	if( $total < $posts_per_page )
		$numerator = $total;
	else
		$numerator = $posts_per_page;

	// Don't go past the upper limit
	if( $offset + $numerator > $total )
	{
		$of         = $total;
		$next_class = 'disable'; // Can't click next button if we're at the uper limit
		$prev_class = '';
	}
	else
	{
		$of         = $offset + $numerator;
		$next_class = '';
	}

	// Disable the previous button if we don't have any previous 
	$prev_class = ( (int)$prev == 0  && (int)$offset == 0 ) ? 'disable' : '';

	// Display the number of where we are in the pagination
	$viewing = $offset.' - '.$of.' of '.$total;

	//
	$html .= '<div class="pagination">';
	$html .= '<span class="current_viewing">'.$viewing.'</span>';
	$html .= '<a href="'.$prev_url.'" class="'.$prev_class.'">Previous</a>';
	$html .= '<a href="'.$next_url.'" class="'.$next_class.'">Next</a>';
	$html .= '</div>';
	// 
	return $html;

}// end func

/**
* Send Onesignal Notification on PostUpdate
*/
function sendMessage( $message ){

	$content = array(
		"en" => $message
	);

	$fields = array(
		'app_id' => ONE_SIGNAL_APP_ID,
		'included_segments' => array('All'),
		//'data' => array( "foo" => "bar" ),
		'contents' => $content
	);

	$fields = json_encode( $fields );
	//print( "\nJSON sent:\n" );
	//print( $fields );
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications" );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic '.ONE_SIGNAL_REST_API_KEY ));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $ch, CURLOPT_HEADER, FALSE );
	curl_setopt( $ch, CURLOPT_POST, TRUE );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );

	$response = curl_exec( $ch );
	curl_close($ch);

	return $response;

} // end func