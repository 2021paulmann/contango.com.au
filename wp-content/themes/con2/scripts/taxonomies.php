<?php

add_action( 'init', 'create_taxonomies' );

function create_taxonomies() {


	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Report Types', 'taxonomy general name' ),
		'singular_name'     => _x( 'Report Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Report Types' ),
		'all_items'         => __( 'All Report Types' ),
		'parent_item'       => __( 'Parent Report Type' ),
		'parent_item_colon' => __( 'Parent Report Type' ),
		'edit_item'         => __( 'Edit Report Type' ),
		'update_item'       => __( 'Update Report Type' ),
		'add_new_item'      => __( 'Add New Report Type' ),
		'new_item_name'     => __( 'New Media Type Name' ),
		'menu_name'         => __( 'Report Type' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => false,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'report_types',
		'reports',
		$args
	);
	
	
	
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Report Period', 'taxonomy general name' ),
		'singular_name'     => _x( 'Report Period', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Report Periods' ),
		'all_items'         => __( 'All Report Periods' ),
		'parent_item'       => __( 'Parent Report Period' ),
		'parent_item_colon' => __( 'Parent Report Period' ),
		'edit_item'         => __( 'Edit Report Period' ),
		'update_item'       => __( 'Update Report Period' ),
		'add_new_item'      => __( 'Add New Report Period' ),
		'new_item_name'     => __( 'New Report Period Name' ),
		'menu_name'         => __( 'Report Period' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'report_periods',
		'reports',
		$args
	);
	
	
	
	
	
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Media Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Media Categories', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Media Categories' ),
		'all_items'         => __( 'All Media Categories' ),
		'parent_item'       => __( 'Parent Media Categories' ),
		'parent_item_colon' => __( 'Parent Media Categories' ),
		'edit_item'         => __( 'Edit Media Categories' ),
		'update_item'       => __( 'Update Media Categories' ),
		'add_new_item'      => __( 'Add New Media Categories' ),
		'new_item_name'     => __( 'New Media Categories Name' ),
		'menu_name'         => __( 'Media Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'media_categories',
		'media_items',
		$args
	);
	
	
	
	
	
	
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Date Periods', 'taxonomy general name' ),
		'singular_name'     => _x( 'Date Period', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Date Periods' ),
		'all_items'         => __( 'All Date Periods' ),
		'parent_item'       => __( 'Parent Date Period' ),
		'parent_item_colon' => __( 'Parent Date Period' ),
		'edit_item'         => __( 'Edit Date Period' ),
		'update_item'       => __( 'Update Date Period' ),
		'add_new_item'      => __( 'Add New Date Period' ),
		'new_item_name'     => __( 'New Date Period Name' ),
		'menu_name'         => __( 'Date Period' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'date_periods',
		'media_items',
		$args
	);



	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Team Roles', 'taxonomy general name' ),
		'singular_name'     => _x( 'Team Role', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Team Roles' ),
		'all_items'         => __( 'All Team Roles' ),
		'parent_item'       => __( 'Parent Team Role' ),
		'parent_item_colon' => __( 'Parent Team Role' ),
		'edit_item'         => __( 'Edit Team Role' ),
		'update_item'       => __( 'Update Team Role' ),
		'add_new_item'      => __( 'Add New Team Role' ),
		'new_item_name'     => __( 'New Team Role Name' ),
		'menu_name'         => __( 'Team Roles' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'team_role',
		'team',
		$args
	);
	
	
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Document Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Document Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Document Categories' ),
		'all_items'         => __( 'All Document Categories' ),
		'parent_item'       => __( 'Parent Document Category' ),
		'parent_item_colon' => __( 'Parent Document Category' ),
		'edit_item'         => __( 'Edit Document Category' ),
		'update_item'       => __( 'Update Document Category' ),
		'add_new_item'      => __( 'Add New Document Category' ),
		'new_item_name'     => __( 'New Document Category Name' ),
		'menu_name'         => __( 'Document Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => false,
		'rewrite'           => false
	);

	register_taxonomy(
		'document_category',
		'documents',
		$args
	);


}