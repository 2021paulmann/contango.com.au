<?php
/**
* @package 2021creative
* Define all REST routes here
* @author: Aakash Dodiya
*/

class RestRoutes{
    
    // constructor
    public function __construct(){
        add_action( 'rest_api_init', array( $this, 'register_routes') );
    }// end func
    
    /**
    * Registering all rest routes
    */
    public function register_routes(){
        // route for team info json
        register_rest_route( 'app/v2', '/team', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_team_json'),
        ));
        // route for report_types json
        register_rest_route( 'app/v2', '/report_types', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_report_types_json'),
        )); 
        // route for announcements json 
        register_rest_route( 'app/v2', '/announcements', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_announcements_json'),
        )); 
        // route for about json 
        register_rest_route( 'app/v2', '/about', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_about_json'),
        )); 
        // route for news json 
        register_rest_route( 'app/v2', '/news', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_news_json'),
        ));
         // route for news json 
        register_rest_route( 'app/v2', '/asset-management', array(
            'methods' => 'GET',
            'callback' => array ( $this, 'get_asset_management_json'),
        ));        
    }// end func
    
    /**
    * Get report types
    */
    public function get_report_types_json(){
        // define an empty array
        $json = array();
        // query arguments
        $terms = get_terms('report_types');
        if($terms){
            // define an array
            $data = array();
            // go through the loop
            foreach($terms as $term){
                // get term id
                $data['id'] = $term->term_id;
                // get term name
                $data['name'] = $term->name;
                
                // add object to an array
                $json[] = $data;
            }            
        }
        
        //return the object
        return $json;
    } // end func
    
    /**
    * Get team posts for api call
    */
    public function get_team_json(){
        // define an empty array
        $json = array();
        // query arguments
        $args = array(
           'post_type' => 'team',
           'posts_per_page' => -1,
           'nopaging' => true,
           'tax_query' => array(
               'relation' => 'OR',
               array(
                    'taxonomy' => 'team_role',
                    'field'    => 'slug',
                    'terms'    => 'mobile-app',
                ),
           )
        );
        // get query object
        $query = new WP_Query($args);
		
        // if have posts
        if($query->have_posts()){
            // define an array
            $data = array();
            // llop through it
            while ( $query->have_posts() ) {
                $query->the_post();
                // get the post title
                $data['name'] = get_the_title();
                // get role
                $data['role'] = get_field('role', get_the_ID());
                // get qualifications
                $data['qualifications'] = get_field('qualifications', get_the_ID());
                // get featured image
                if (has_post_thumbnail( get_the_ID() )) {
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' ); 
                    $data['photo'] = (isset($image[0]) ? $image[0] : '');
                }
                // get description
                $data['biography'] = get_field('left_column', get_the_ID()) . get_field('right_column', get_the_ID());
               
                // add object to an array
                $json[] = $data;
            }
        }
        //reset post_data
        wp_reset_postdata();
        //reset query
        wp_reset_query();
        
        //return the object
        return $json;        
        
    }// end func
    
    /**
    * Get all announcement
    */
    public function get_announcements_json(){
        // define an empty array
        $json = array();
        // query arguments
        $args = array(
           'post_type' => 'reports',
           'posts_per_page' => -1,
           'nopaging' => true,
          // 'category_name' => 'asx-announcements' 
        );
        // get query object
        $query = new WP_Query($args);
        // if have posts
        if($query->have_posts()){
            // define an array
            $data = array();
            // llop through it
            while ( $query->have_posts() ) {
                $query->the_post();
                // get the post title
                $data['title'] = get_the_title();
                // get company
                $data['company'] = strtoupper(get_field('company', get_the_ID()));
                // get report_types
                $terms = get_the_terms( get_the_ID(), 'report_types' );
                if ( $terms && ! is_wp_error( $terms ) ) {
                    $term_array = array();
                    foreach($terms as $term){
                        $term_array[] = $term->term_id; 
                    }
                    // make a string
                    $terms = implode(",", $term_array); 
                }
                 
                $data['report_type'] = $terms;
                // get  the date
                $data['date'] = get_the_date('Y-m-d\TH:i:s\Z', get_the_ID());
                // get content type
                $file = get_field('file', get_the_ID());              
                $data['content_type'] = strtoupper( $this->_get_file_extension($file) );
                // get file
                $data['url'] = $file;
                // add object to an array
                $json[] = $data;
            }
        }
        //reset post_data
        wp_reset_postdata();
        //reset query
        wp_reset_query();
        
        //return the object
        return $json;   
    }
    
    /**
    * Get about json
    */
    public function get_about_json(){
        // define an empty array
        $json = array();
        // query arguments
        $args = array(
           'post_type' => 'listed_companies',
           'posts_per_page' => -1,
           'nopaging' => true
        );
        // get query object
        $query = new WP_Query($args);
        // if have posts
        if($query->have_posts()){
            // define an array
            $data = array();
            // llop through it
            while ( $query->have_posts() ) {
                $query->the_post();
                // get the company code
                $data['title'] = get_field( 'code', get_the_ID() );
                // get company title
                $data['subtitle'] = get_the_title();
                // get content
                $data['content'] = get_the_content();
               
                // add object to an array
                $json[] = $data;
            }
        }
        //reset post_data
        wp_reset_postdata();
        //reset query
        wp_reset_query();
        
        //return the object
        return $json;        
    }// end func
    
    
    /**
    * Get news json
    */
    public function get_news_json(){
        // define an empty array
        $json = array();
        // query arguments
        $args = array(
           'post_type' => 'media_items',
           'posts_per_page' => -1,
           'nopaging' => true
        );
        // get query object
        $query = new WP_Query($args);
        // if have posts
        if($query->have_posts()){
            // define an array
            $data = array();
            // llop through it
            while ( $query->have_posts() ) {
                $query->the_post();
                // get the title
                $data['title'] = get_the_title();
                // get company title
                //$data['company'] = 
                // get the date
                $data['date'] = get_the_date('Y-m-d\TH:i:s\Z', get_the_ID());
                // get content type & url of it
                $type = get_field( 'type', get_the_ID() );
                $url = '';
                if($type){
                    switch($type){
                        case 'link':
                            $type = 'external_link';
                            $url = get_field( 'link', get_the_ID() );
                        break;
                        case 'video':
                            $url = get_field( 'video_link', get_the_ID() );
                        break;
                        case 'pdf':
                            $url = get_field( 'pdf', get_the_ID() );
                        break;
                    }                 
                    
                }
                
                $data['content_type'] = strtoupper($type);
                
                $data['url'] =   $url; 
                //get content
                $data['content'] = get_the_excerpt();
                // add object to an array
                $json[] = $data;
            }
        }
        //reset post_data
        wp_reset_postdata();
        //reset query
        wp_reset_query();
        
        //return the object
        return $json; 
    }// end func
    
    /**
    * Get Asset Managemtn page json
    */
    public function get_asset_management_json(){
        // define an empty array
        $json = array();
       // query arguments
        $args = array(
           'page_id' => 6,
        );
        // get query object
        $query = new WP_Query($args);
        // if have posts
        if($query->have_posts()){
            // define an array
            $data = array();
            // llop through it
            while ( $query->have_posts() ) {
                $query->the_post();
                // get the title
                $data['title'] = get_the_title();
                // get the date
                $data['date'] = get_the_date('Y-m-d\TH:i:s\Z', get_the_ID());
                //get content
                $data['content'] = get_field('json_text', get_the_ID() );
                // add object to an array
                $json[] = $data;
            }
        }
        //reset post_data
        wp_reset_postdata();
        //reset query
        wp_reset_query();
        
        return $json;
    }// end func
    
    
    /**
    * Get file extension
    */
    private function _get_file_extension($file_name) {
	    return substr( strrchr( $file_name , '.' ), 1 );
    }// end func
    
}

// initiate class here
new RestRoutes();