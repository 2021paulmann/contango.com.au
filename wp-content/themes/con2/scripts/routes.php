<?php
/*
	
*/


function add_query_vars($vars) 
{
    $vars[] = "member_id"; //
	$vars[] = "report_type";
	$vars[] = "date_period";
	$vars[] = "company";
	$vars[] = "offset";
	$vars[] = "key";

    return $vars;
}
add_filter('query_vars', 'add_query_vars');

function add_rewrite_rules($aRules) 
{
	$aNewRules = array( 
		'^investment-team/([^/]+)/?$'       => 'index.php?pagename=investment-team&member_id=$matches[1]',
		'^board-governance/([^/]+)/?$'      => 'index.php?pagename=board-governance&member_id=$matches[1]',
		'^cga-board/([^/]+)/?$'      		=> 'index.php?pagename=cga-board&member_id=$matches[1]',
		'^import/([^/]+)/?$'        		=> 'index.php?pagename=import&key=$matches[1]'
	);
    $aRules = $aNewRules + $aRules;
    return $aRules;
}
add_filter('rewrite_rules_array', 'add_rewrite_rules');

