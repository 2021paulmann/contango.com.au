<?php
	
add_shortcode( 'current_share_price',  'get_current_share_price' );
add_shortcode( 'fund_size',            'get_fund_size' );	
add_shortcode( 'net_assets_per_share', 'get_net_assets_per_share' );	
add_shortcode( 'annualised_dividend',  'get_annualised_dividend' );	
add_shortcode( 'dividend_yield',       'get_dividend_yield' );	
add_shortcode( 'current_datetime',     'get_current_datetime' );	
add_shortcode( 'market_cap',           'get_market_cap' );	


function get_current_share_price($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return $data->quote[0]->lastprice;
}

function get_fund_size($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return round(($data->quote[0]->fundsize /1000000),2);
}


function get_net_assets_per_share($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return $data->quote[0]->netassets;
}

function get_market_cap($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return round(($data->quote[0]->marketcap/1000000),2);
}
	
	
function get_annualised_dividend($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return $data->quote[0]->annualiseddividend;
}
	
	
function get_dividend_yield($atts)
{
	$feed = $atts['company'].'_price';
	$data = get_feed($feed, true);
	
	return $data->quote[0]->dividendyield;
}


function get_current_datetime($atts)
{
	date_default_timezone_set('Australia/Melbourne');
	
	$minus_20 = time() - (60*20);
	$date = date("F j, Y, g:i a", $minus_20) . ' AEST';
	return $date;
}