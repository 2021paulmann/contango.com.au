<?php

function register_menus() 
{
	register_nav_menu('upper-header-menu',     'Upper Header Menu');
	register_nav_menu('main-menu',             'Main Menu');
	register_nav_menu('footer-menu-one',       'Footer Menu One');
	register_nav_menu('footer-sub-menu',       'Footer Sub Menu');
	register_nav_menu('asset-management-menu', 'Asset Management Menu');
	register_nav_menu('mobile-menu',           'Mobile Menu');
	register_nav_menu('listed-companies-menu', 'Listed Companies Menu');
	register_nav_menu('microcap-menu',         'Microcap Menu');
	register_nav_menu('income-generator-menu', 'Income Generator Menu');
	register_nav_menu('investor-centre-menu', 'Investor Centre Menu');
	register_nav_menu('global-growth-menu', 'Global Growth Menu');
}
add_action( 'init', 'register_menus' );