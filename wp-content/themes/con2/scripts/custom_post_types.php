<?php

add_action( 'init', 'post_type_media_items' );

function post_type_media_items() 
{
 	$labels = array(
    	'name'                => 'Media Items',
    	'singular_name'       => 'Media Item',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Media Item',
    	'edit_item'           => 'Edit Media Items',
    	'new_item'            => 'New Media Item',
    	'all_items'           => 'All Media Items',
    	'view_item'           => 'View Media Items',
    	'search_items'        => 'Search Media Items',
    	'not_found'           => 'No Media Items found',
    	'not_found_in_trash'  => 'No Media Items found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Media Items'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
    	'show_in_menu'        => true, 
    	'query_var'           => false,
    	'rewrite'             => false,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
		'show_in_rest'        => true,
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title', 'editor', 'thumbnail' )
  ); 
  register_post_type( 'media_items', $args );
}




add_action( 'init', 'post_type_listed_companies' );

function post_type_listed_companies() 
{
 	$labels = array(
    	'name'                => 'Listed Companies',
    	'singular_name'       => 'Listed Company',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Listed Company',
    	'edit_item'           => 'Edit Company',
    	'new_item'            => 'New Company',
    	'all_items'           => 'All Listed Companies',
    	'view_item'           => 'View Listed Companies',
    	'search_items'        => 'Search Listed Companies',
    	'not_found'           => 'No Companies found',
    	'not_found_in_trash'  => 'No Companies found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Listed Companies'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
    	'show_in_menu'        => true, 
    	'query_var'           => false,
    	'rewrite'             => false,
		'show_in_rest'        => true,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title', 'editor', 'thumbnail' )
  ); 
  register_post_type( 'listed_companies', $args );
}




add_action( 'init', 'post_type_reports' );

function post_type_reports() 
{
 	$labels = array(
    	'name'                => 'Reports',
    	'singular_name'       => 'Report',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Report',
    	'edit_item'           => 'Edit Report',
    	'new_item'            => 'New Report',
    	'all_items'           => 'All Reports',
    	'view_item'           => 'View Reports',
    	'search_items'        => 'Search Reports',
    	'not_found'           => 'No Reports found',
    	'not_found_in_trash'  => 'No Reports found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Reports'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
    	'show_in_menu'        => true, 
    	'query_var'           => false,
    	'rewrite'             => false,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
		'show_in_rest'        => true,
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title', 'thumbnail' )
  ); 
  register_post_type( 'reports', $args );
}






add_action( 'init', 'post_type_team' );

function post_type_team() 
{
 	$labels = array(
    	'name'                => 'Team',
    	'singular_name'       => 'Team',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Member',
    	'edit_item'           => 'Edit Member',
    	'new_item'            => 'New Member',
    	'all_items'           => 'All Members',
    	'view_item'           => 'View Members',
    	'search_items'        => 'Search Members',
    	'not_found'           => 'No Members found',
    	'not_found_in_trash'  => 'No Members found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Team'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
    	'show_in_menu'        => true, 
    	'query_var'           => false,
		'show_in_rest'        => true,
    	'rewrite'             => false,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title', 'thumbnail' )
  ); 
  register_post_type( 'team', $args );
}





add_action( 'init', 'post_type_documents' );

function post_type_documents() 
{
 	$labels = array(
    	'name'                => 'Documents',
    	'singular_name'       => 'Document',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Document',
    	'edit_item'           => 'Edit Document',
    	'new_item'            => 'New Document',
    	'all_items'           => 'All Documents',
    	'view_item'           => 'View Documents',
    	'search_items'        => 'Search Documents',
    	'not_found'           => 'No Documents found',
    	'not_found_in_trash'  => 'No Documents found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Documents'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
    	'show_in_menu'        => true, 
		'show_in_rest'        => true,
    	'query_var'           => false,
    	'rewrite'             => false,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title' )
  ); 
  register_post_type( 'documents', $args );
}




add_action( 'init', 'post_type_research' );

function post_type_research() 
{
 	$labels = array(
    	'name'                => 'Research Documents',
    	'singular_name'       => 'Research Document',
    	'add_new'             => 'Add New',
    	'add_new_item'        => 'Add New Research Document',
    	'edit_item'           => 'Edit Research Document',
    	'new_item'            => 'New Research Document',
    	'all_items'           => 'All Research Documents',
    	'view_item'           => 'View Research Documents',
    	'search_items'        => 'Search Research Documents',
    	'not_found'           => 'No Research Documents found',
    	'not_found_in_trash'  => 'No Research Documents found in Trash', 
    	'parent_item_colon'   => '',
    	'menu_name'           => 'Research Documents'
  );
  $args = array(
		'labels'              => $labels,
    	'public'              => true,
    	'publicly_queryable'  => false,
    	'show_ui'             => true, 
		'show_in_rest'        => true,
    	'show_in_menu'        => true, 
    	'query_var'           => false,
    	'rewrite'             => false,
    	'capability_type'     => 'page',
    	'has_archive'         => false, 
    	'hierarchical'        => false,
    	'menu_position'       => null,
    	'supports'            => array( 'title', 'excerpt' )
  ); 
  register_post_type( 'research_documents', $args );
}

