<?php
/**
 * Register our sidebars and widgetized areas.
*
*/

add_action( 'widgets_init', 'widgets_register' );
function widgets_register() 
{
	register_sidebar( array(
		'name'           => 'Homepage Panel One',
		'id'             => 'homepage_panel_one',
		'before_widget'  => '',
		'after_widget'   => '',
		'before_title'   => '<span style="display: none;">',
		'after_title'    => '</span>'
		) 
	);
	
	register_sidebar( array(
		'name'           => 'Footer Panel Four',
		'id'             => 'footer_panel_four',
		'before_widget'  => '',
		'after_widget'   => '',
		'before_title'   => '<span style="display: none;">',
		'after_title'    => '</span>'
		) 
	);


}
