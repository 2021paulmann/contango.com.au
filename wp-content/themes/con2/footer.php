		<footer>
			<div class="container_white" style="padding-top:5px;">
				<div class="column one_col column_one">
					<div class="left">&copy; CONTANGO ASSET MANAGEMENT LIMITED</div>
				</div>
				
				<div class="column one_col column_two">
					<?php echo do_shortcode('[gravityforms id="1" title="false" description="false" ajax="true"]');?>

				</div>

				<div class="column one_col column_three">
					<div class="footer-social">		
						<a href="https://www.linkedin.com/company/contango-asset-management-limited/" target="_blank"><img src="<?php get_template_directory_uri() ?>/wp-content/themes/con/images/linked-in.svg"></a>
						<a href="https://twitter.com/contangoam/" target="_blank"><img src="<?php get_template_directory_uri() ?>/wp-content/themes/con/images/twitter.svg"></a>
						<a href="https://www.youtube.com/channel/UCMmEslrXnFOFxSmGQZIyrVg" target="_blank"><img src="<?php get_template_directory_uri() ?>/wp-content/themes/con/images/youtube.svg"></a>
					</div>
					<ul class="footer_menu">
						<?php
							$defaults = array(
								'theme_location'  => 'Footer Menu One',
								'menu'            => 'footer-menu-one',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '%3$s',
								'depth'           => 0,
								'walker'          => ''
							);
							wp_nav_menu( $defaults ); 
						?>
					</ul>
				</div>

				<div class="clear"></div>
			 </div>
		 </footer>
		 
  
	  	<script src="<?php echo JS_PATH; ?>tools.js"></script>	
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		<script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>	
	  	<script src="<?php echo JS_PATH; ?>scripts.js"></script>
		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-36225731-1', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->
		<?php wp_footer(); ?>
	</body>
</html>