(function($)
{
	
	/*
	
	*/
	$('.slider_button').on('click', function(e)
	{
		e.preventDefault();

		var slider = $('.asx_slider'),
			button = $('a.slider_button').find('img');
		
		// if closed
		if( slider.hasClass('open'))
		{
			slider.removeClass('open');
			var new_src = button.attr('data-closed-src');
			button.attr('src', new_src);
		}
		else
		{	
			slider.addClass('open');
			var new_src = button.attr('data-open-src');
			button.attr('src', new_src);
		}
	});
	
	
	
	
	
	/*
	
	*/
	$('nav#main ul li').hover(function(e)
	{
		$(this).find('.sub-menu').show();
	}, 
	function(e)
	{
		$(this).find('.sub-menu').hide();
	});
	
	
	
	
	/*
	
	*/
	$('.member').hover(function(e)
	{
		$(this).find('.more_info').css('display', 'block');
	}, 
	function(e)
	{
		$(this).find('.more_info').css('display', 'none');
		
	});
	
	
	
	/*
	
	*/
	$('.mobile_menu').on('click', function(e)
	{
		e.preventDefault();

		var menu = $('#mobile_menu');
		
		menu.toggle();

	});
	
	
	
	/*
	
	*/
	$('#mobile_menu ul li.menu-item-has-children > a').on('click', function(e)
	{
		e.preventDefault();
		
		var submenu = $(this).next('.sub-menu');
		
		if( submenu.is(':visible') )
		{
			$(this).removeClass('open');
			submenu.hide();
		}
		else
		{
			$(this).addClass('open');
			submenu.show();
		}
	});
	
	
	
	$('.no_click > a').on('click', function(e)
	{
		e.preventDefault();
	});
	
	
	$('.disable').on('click', function(e)
	{
		e.preventDefault();
	});
	
	
	
	
	$("select").selectBoxIt();
	
	$('#reports').stacktable();
	
	
})(jQuery);