<!doctype html>

<html lang="en">
	<head>
	  	<meta charset="utf-8">

	  	<title>Contango Asset Management</title>
	  	<meta name="description" content="">
	  	<meta name="author"      content="">
		<meta name="viewport"    content="initial-scale=device-width,maximum-scale=1">
		
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,700" rel="stylesheet" type="text/css">
	  
		<link type="text/css" rel="stylesheet" href="http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />
		
		<link rel="icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon" />
  
  	  	<?php wp_head(); ?>
  	  	
	 	 <!--[if lt IE 9]>
	 		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	 	<![endif]-->
  	<?php 
  		// 
		global $post;
		$body_class = $post->post_name;
		
		// Had to change the name of the media page, which affects CSS attached to that specific page
		if( $body_class == 'latest-media')
			$body_class = 'media';
			
		$company    = get_page_section_company($post, 'none');
	?>
</head>

<body class="<?php echo $body_class;  ?>">
	<header>
	
		<div id="slider-wrap">
		<div class="head-container">
			<a class="logo" href="<?php echo site_url(); ?>">
				
				<?php
				switch( $company )
				{
					case 'ctn':
						echo '<img src="'.IMAGES_PATH.'microcap_logo.svg" width="197px" alt="microcap-logo" />';
					break;
					case 'cie':
						echo '<img src="'.IMAGES_PATH.'income_generator_logo.svg" width="203px" alt="income-generator-logo" />';
					break;
					case 'aml':
						echo '<img src="'.IMAGES_PATH.'caml_logo.svg" width="211px" alt="asset-management-logo" />';
					break;
					case 'cqg':
						echo '<img src="'.IMAGES_PATH.'wcm-global-growth-logo.svg" height="42.3px" alt="global-growth-logo" />';
					break;
					default:
						echo '<img src="'.IMAGES_PATH.'caml_logo.svg" width="211px" alt="asset-management-logo" />';
					break;
				}
				?>
			</a>
			
			<a href="#" class="mobile_menu"><img src="<?php echo IMAGES_PATH; ?>mobile_menu.png" ></a>
			<div id="mobile_menu">
				<ul>
				<?php
					$defaults = array(
						'theme_location'  => 'Mobile Menu',
						'menu'            => 'mobile-menu',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '%3$s',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults ); 
				?>
			</ul>
			</div>
			
				
			<nav id="main">
				<ul>
					<?php
						$defaults = array(
							'theme_location'  => 'Main Menu',
							'menu'            => 'main-menu',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '%3$s',
							'depth'           => 0,
							'walker'          => ''
						);
						wp_nav_menu( $defaults ); 
					?>
				</ul>
			</nav>
			
			<nav id="top_right">
				<ul>
					<?php
						$defaults = array(
							'theme_location'  => 'Upper Header Menu',
							'menu'            => 'upper-header-menu',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '%3$s',
							'depth'           => 0,
							'walker'          => ''
						);
						wp_nav_menu( $defaults ); 
					?>
				</ul>
			</nav>
			
			
			<div class="asx_slider">
				<a href="#" class="slider_button"><img src="<?php echo IMAGES_PATH;?>asx-in.gif" data-open-src="<?php echo IMAGES_PATH;?>asx-out.gif" data-closed-src="<?php echo IMAGES_PATH;?>asx-in.gif"></a>
				<div class="asx_content">
					<?php echo indices_table( "S&amp;P/ASX INDICES", 'asx_indices' ); ?>
					
					<?php echo indices_table( "WORLD INDICES", 'world_indices' ); ?>
					
					<?php echo indices_table( "METALS &amp; CURRENCIES", 'metal_currency' ); ?>

				</div>
				</div>
			
			</div>
		</div>
		</div>

	</header>








