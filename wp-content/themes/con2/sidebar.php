<aside id="sidebar">
<?php
	
global $post;
 
//
$microcap_generator_id = 38;
$income_generator_id   = 40;
$listed_companies_id   = 8;
$investor_centre_id	   = 12877;
$global_growth_id	   = 14112;

if( $post->ID == $listed_companies_id ): ?>
<h4 class="orange"><a href="<?php echo site_url();?>/investments/">INVESTMENTS</a></h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Listed Companies Menu',
			'menu'            => 'listed-companies-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>


<?php elseif( $post->post_parent == $microcap_generator_id || $post->ID == $microcap_generator_id ): ?>
<h4 class="orange"><a href="<?php echo site_url();?>/investments/microcap-limited/">CONTANGO MICROCAP LIMITED</a></h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Microcap Menu',
			'menu'            => 'microcap-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>

<?php elseif( $post->post_parent == $income_generator_id || $post->ID == $income_generator_id ): ?>

<h4 class="orange"><a href="<?php echo site_url();?>/income-generator-limited/">CONTANGO INCOME GENERATOR LIMITED</a></h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Income Generator Menu',
			'menu'            => 'income-generator-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>

<?php elseif( $post->post_parent == $global_growth_id || $post->ID == $global_growth_id ): ?>

<h4 class="orange"><a href="<?php echo site_url();?>/global-growth">WCM GLOBAL GROWTH LIMITED (ASX:WQG)</a></h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Global Growth Menu',
			'menu'            => 'global-growth-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>

<?php elseif( $post->post_parent == $investor_centre_id || $post->ID == $investor_centre_id ): ?>

<h4 class="ic_sidebar"><a href="<?php echo site_url();?>/investor-centre">INVESTOR CENTRE (ASX:CGA)</a></h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Investor Centre Menu',
			'menu'            => 'investor-centre-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>

<?php else: ?>
	
<h4 class="orange">CONTANGO ASSET MANAGEMENT LIMITED</h4>	
<ul class="asset_management_menu">
	<?php
		$defaults = array(
			'theme_location'  => 'Asset Management Menu',
			'menu'            => 'asset-management-menu',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		);
		wp_nav_menu( $defaults ); 
	?>
</ul>

<?php endif; ?>	


	
	

</aside>